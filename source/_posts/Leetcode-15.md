---
title: Leetcode-15-三数之和
catalog: true
date:  2018-11-24
top_img: https://i.loli.net/2018/11/24/5bf8f122cbdd2.jpg
tags:
    - 算法
---

## 题目
给定一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？找出所有满足条件且不重复的三元组。

注意：答案中不可以包含重复的三元组。

```
例如, 给定数组 nums = [-1, 0, 1, 2, -1, -4]，

满足要求的三元组集合为：
[
  [-1, 0, 1],
  [-1, -1, 2]
]
```

## 解法
```
var threeSum = function (nums) {
  var res = [];
  nums.sort(function (a, b) {
    return a - b;
  });
  for (var i = 0; i < nums.length - 1; i++) {
    // 避免重复
    if (i > 0 && nums[i] === nums[i - 1]) {
      continue;
    }
    var l = i + 1;
    var r = nums.length - 1;
    while (l < r) {
      var s = nums[i] + nums[l] + nums[r];
      if (s < 0) {
        l++;
      } else if (s > 0) {
        r--;
      } else {
        res.push([nums[i], nums[l], nums[r]]);
        // l < r || r > l 证明中间还有数, 如果下一个数跟这个数相等，就要去重复。
        while (l < r && nums[l] === nums[l + 1]) {
          l++;
        }
        while (r > l && nums[r] === nums[r - 1]) {
          r++;
        }
        // 继续
        l++;
        r--;
      }
    }
  }
  return res;
};
```