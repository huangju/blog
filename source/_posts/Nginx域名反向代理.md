---
title: Nginx域名反向代理
catalog: true
date:  2018-12-19
top_img: https://i.loli.net/2018/12/20/5c1b0fcc1dbc6.jpg
tags:
    - Nginx
---

### 1、新建并启动nginx容器

```
docker run –name=nginx -p 80:80 -v /nginx/conf.d:/etc/nginx/conf.d -d nginx
```
``/etc/nginx/conf.d``文件跟主机的``/etc/nginx/conf.d``文件同步


### 2、新建配置文件

```
cd /nginx/conf.d
```

```
名字必须为.conf格式
vi www.xxx.com.conf
```
```
//编写文件
//按ESC 输入:wq保存文件

server {
    listen       80;
    server_name www.xxx.com;
    location / {
       proxy_pass http://宿主机ip:容器对外的端口号;
      }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```
重启nginx容器
```
docker restart nginx
```

---
### BUG:遇到502

原因是没有开启端口

```
firewall-cmd --zone=public --add-port=容器对外的端口号/tcp --permanent
```

重启防火墙
```
firewall-cmd --reload
```

