---
title: CodePush热更新
catalog: true
date:  2018-11-30
top_img: https://i.loli.net/2018/11/30/5c00cf0783761.jpg
tags:
    - React Native
---

> demo:https://gitlab.com/huangju/codepush

### 全局安装code-push-cli
```
npm install -g code-push-cli
```

### 注册code-push账号
```
code-push register
//会弹出一个网页，登陆授权成功后有token复制
//3784e1126be3198319a827b765******
//把token复制到终端
```
![image](https://i.loli.net/2018/11/30/5c00da98d5d35.png)

![image](https://i.loli.net/2018/11/30/5c00da98bf859.png)

![image](https://i.loli.net/2018/11/30/5c00da98c4d9c.png)

### 在CodePush服务器注册app

注册ios版
```
code-push app add <appName> <os> <platform>
例如:code-push app add codepush-ios ios react-native
//成功返回
Successfully added the "codepush-ios" app, along with the following default deployments:
┌────────────┬──────────────────────────────────────────────────────────────────┐
│ Name       │ Deployment Key                                                   │
├────────────┼──────────────────────────────────────────────────────────────────┤
│ Production │ nD93HCWVCaLfftzY0P2Jp3DQnygJ171dec58-5ea7-4b60-8430-4e3a6d3995b0 │
├────────────┼──────────────────────────────────────────────────────────────────┤
│ Staging    │ iBIGzo7tThfAF4Org0ZhmbMsr0Jz171dec58-5ea7-4b60-8430-4e3a6d3995b0 │
└────────────┴──────────────────────────────────────────────────────────────────┘
```
注册android版
```
code-push app add codepush-android android react-native
//成功返回
Successfully added the "codepush-android" app, along with the following default deployments:
┌────────────┬──────────────────────────────────────────────────────────────────┐
│ Name       │ Deployment Key                                                   │
├────────────┼──────────────────────────────────────────────────────────────────┤
│ Production │ d5eKJHS8fGHjt7zHRpgl7-Oh3-sr171dec58-5ea7-4b60-8430-4e3a6d3995b0 │
├────────────┼──────────────────────────────────────────────────────────────────┤
│ Staging    │ l1HbBGQmnwlJtzY7mMvs3teAeHNa171dec58-5ea7-4b60-8430-4e3a6d3995b0 │
└────────────┴──────────────────────────────────────────────────────────────────┘
```

### 集成CodePush SDK
```
yarn add react-native-code-push
react-native link react-native-code-push

//然后需要填入前面的Staging的key
? What is your CodePush deployment key for Android (hit <ENTER> to ignore)
//Android的Staging的key
? What is your CodePush deployment key for iOS (hit <ENTER> to ignore)
//iOS的Staging的key
```

### APP.js添加代码
```
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import codePush from "react-native-code-push";
const codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };

export default class App extends Component {

  componentDidMount() {
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
      mandatoryInstallMode: codePush.InstallMode.IMMEDIATE,
      //deployment是刚才生成的Production的key
      //用Platform判断下平台
      deploymentKey: Platform.OS === 'ios' ? 'nD93HCWVCaLfftzY0P2Jp3DQnygJ171dec58-5ea7-4b60-8430-4e3a6d3995b0' : 'd5eKJHS8fGHjt7zHRpgl7-Oh3-sr171dec58-5ea7-4b60-8430-4e3a6d3995b0',
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Hello World</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
```
### 运行项目开启debug调试
显示这个就是正常的了

![image](https://i.loli.net/2018/11/30/5c00db0563333.png)


### 测试-发布一个新版本
```
测试环境 Staging
//在测试环境下，感觉没效果，应该是测试环境会自动更新。
-------------------------
code-push release-react <appName> <platform>
code-push release-react codepush-ios ios
code-push release-react codepush-android android

生产环境 Production
-------------------------
code-push release-react codepush-android android --t 1.0.0 --d Production --des "优化" --m true

参数说明
-------------------------
--t:安装包的版本
--d:要发布更新的环境分Production与Staging(默认为Staging)
--des:更新说明
--m:强制更新
--dev:是否启用开发者模式(默认为false)
```

### 其他一些命令
``code-push deployment ls codepush-android``

![image](https://i.loli.net/2018/11/30/5c00db05197b6.png)


### 出现的问题
在注册app的时候``code-push app add codepush-ios ios react-native``

![image](https://i.loli.net/2018/11/30/5c00db05139e3.png)
重新试一次，或者新开一个终端尝试