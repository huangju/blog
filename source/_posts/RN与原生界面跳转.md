---
title: RN与原生界面跳转
catalog: true
date:  2018-11-26
top_img: https://i.loli.net/2018/11/26/5bfbf7b4149de.jpg
tags:
    - React Native
---

> DEMO https://gitlab.com/huangju/rn-push-native-demo

## Android
### 新建一个Activity
```
//TestActivity
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class TestActivity extends AppCompatActivity {

    private Button btnGoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        btnGoto = findViewById(R.id.btn_goto);
        btnGoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //原生跳转到RN
                Intent intent = new Intent(TestActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
```

```
//activity布局

<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical" android:layout_width="match_parent"
    android:layout_height="match_parent">

    <Button
        android:id="@+id/btn_goto"
        android:text="点击跳转"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />
</LinearLayout>
```

### 新建一个Module
```
//TestModule
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class TestModule extends ReactContextBaseJavaModule {
    public TestModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "IntentModule";
    }

    @ReactMethod
    public void startActivityFromJS(String name, String params) {
        try {
            Activity currentActivity = getCurrentActivity();
            if (null != currentActivity) {
                Class toActivity = Class.forName(name);
                Intent intent = new Intent(currentActivity, toActivity);
                intent.putExtra("params", params);
                currentActivity.startActivity(intent);
            }
        } catch (Exception e) {
            throw new JSApplicationIllegalArgumentException(
                    "不能打开Activity:" + e.getMessage()
            );
        }
    }

    @ReactMethod
    public void dataToJs(Callback successBack, Callback errorBack) {
        try {
            Activity currentActivity = getCurrentActivity();
            String result = currentActivity.getIntent().getStringExtra("data");
            if (TextUtils.isEmpty(result)) {
                result = "没有数据";
            }
            successBack.invoke(result);
        } catch (Exception e) {
            errorBack.invoke(e.getMessage());
        }
    }
}
```

### 新建一个Package

```
//TestPackage
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestPackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        return Arrays.<NativeModule>asList(new TestModule(reactContext));
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
```
### 注册Activity
```
//AndroidManifest

    <application
      android:name=".MainApplication"
      android:label="@string/app_name"
      android:icon="@mipmap/ic_launcher"
      android:allowBackup="false"
      android:theme="@style/AppTheme">
      <activity
        android:name=".MainActivity"
        android:label="@string/app_name"
        android:configChanges="keyboard|keyboardHidden|orientation|screenSize"
        android:windowSoftInputMode="adjustResize">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
      </activity>
      <activity android:name="com.facebook.react.devsupport.DevSettingsActivity" />
      <!--注册TestActivity-->
        <activity android:name=".TestActivity"/>
    </application>
```

### RN调用
```
import React, { Component } from 'react';
import { StyleSheet, Text, NativeModules, View, TouchableOpacity } from 'react-native';


export default class App extends Component {

  _goTo = () => {
    NativeModules
      .IntentModule
      .startActivityFromJS("com.test.TestActivity", null);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this._goTo}>
          <Text>跳转原生界面</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

```

## IOS
### 创建原生路由
```
//AppDelegate.h
//创建一个原生的导航条
@property (nonatomic, strong) UINavigationController *navController;
```

### 初始化路由
```
//AppDelegate.h
_navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
self.window.rootViewController = _navController;
```
![image.png](https://i.loli.net/2018/11/26/5bfbf8678f3ed.png)

### 创建给RN的Module类
```
//PushNative.h
#import <Foundation/Foundation.h>
// 导入RCTBridgeModule类，这个是react-native提供
#import <React/RCTBridgeModule.h>
// 遵守RCTBridgeModul协议
@interface PushNative : NSObject<RCTBridgeModule>
@end


```

```
//PushNative.m
#import "PushNative.h"
// 导入AppDelegate，获取UINavigationController
#import "AppDelegate.h"
// 导入跳转的页面
#import "TestViewController.h"

@implementation PushNative

//导出模块
RCT_EXPORT_MODULE();
// RN跳转原生界面
// rnToNative导出方法
RCT_EXPORT_METHOD(rnToNative) {
  //主要这里必须使用主线程发送,不然有可能失效
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *rootNav = app.navController;

    TestViewController *test = [[TestViewController alloc] init];
    
    [rootNav pushViewController:test animated:YES];
  });
}

@end


```



### 创建原生界面
```
//TestViewController.h
#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController

@end

```

```
//TestViewController.m
#import "TestViewController.h"
@implementation TestViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = @"这是原生页面";
  }
@end
```

### RN调用
```
import React from 'react';
import {View, Text, Button, NativeModules} from 'react-native';

var nativeModule = NativeModules.OpenNativeModule;

export default class HomeScreen extends React.Component {
  
  render() {
    return (
      <View>
        <Text>首页</Text>
        <Button title={'跳转到原生页面'} onPress={() => {
          this.jumpToNativeView();
        }}/>
      </View>
    )
  }
  
  jumpToNativeView() {
    //PushNative是iOS导出的模块
    //rnToNative是iOS导出的方法
    NativeModules.PushNative.rnToNative();
  }
}

```