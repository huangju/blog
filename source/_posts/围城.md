---
title: 围城
catalog: true
date:  2019-1-20
top_img: https://i.loli.net/2019/01/20/5c449ac5a932e.jpg
tags:
    - 书籍
---

大多数人是方鸿渐

软弱无能，家世一般，不中用的傲气

有看不起的人，也有看不到的人

生活磨平你的凌角，给你一段对生活充满妥协的婚姻，处处都是琐碎争吵

你本来可以改变这人生轨迹，在留学时可以努力拼搏获得博士学位，在上海可以不与周家翻脸，你可以向你的红玫瑰解释，可以在三闾大学留下来

可你没有，你终究是不中用的无能

婚姻是围城，人生也是围城

被困在围城中何止你一人，世俗之人都困于围城中

我也在进城的路上

我终究属于大多数人
