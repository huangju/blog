---
title: Centos开启BBR
catalog: true
date:  2018-12-22
top_img: https://i.loli.net/2018/12/22/5c1e51104da99.jpg
tags:
    - Centos
---


### 1、查看当前核心
```
uname -r 
```
![image](https://i.loli.net/2018/12/24/5c20fe944af3c.png)

不是4以上是不支持BBR的

### 2、更新内核
```
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
```

```
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
```

### 3、安装最新稳定内核
```
yum --enablerepo=elrepo-kernel install kernel-ml -y
```

### 4、检查内核是否更新
```
rpm -qa | grep kernel
```
![image](https://i.loli.net/2018/12/24/5c20feb2b197f.png)

检查是否存在4.9版本内核

**4.9版本是位于0，从下往上数**

### 5、设置内核
```
vi /etc/default/grub
```
把``GRUB_DEFAULT=saved``修改为``GRUB_DEFAULT=0``

![image](https://i.loli.net/2018/12/24/5c20fed05f5c9.png)

**0是你想设置的内核位置，从下开始数，从0开始数**

生成新的内核配置
```
grub2-mkconfig -o /boot/grub2/grub.cfg
```

### 6、重启

```
shutdown -r now
```

### 7、验证内核
```
uname -r
```
![image](https://i.loli.net/2018/12/24/5c20feec120d8.png)

### 8、开启BBR算法
```
echo 'net.core.default_qdisc=fq' | sudo tee -a /etc/sysctl.conf
```
```
echo 'net.ipv4.tcp_congestion_control=bbr' | sudo tee -a /etc/sysctl.conf
```
```
sysctl -p
```

### 9、验证BBR是否成功
```
lsmod | grep bbr
```
![image](https://i.loli.net/2018/12/24/5c20fef9e7ba2.png)